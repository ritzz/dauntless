﻿; Author: RitZ
; Purpose: This script repeatedly auto clicks. It was requested by a person with disability issues.
;		   Press Left click to start auto left click, Press Middle click to stop it
;		   Press Right click to start auto right click, Press End button to stop it
; 		   You need to stop the auto clicks first, then Press F6 to suspend the script, so that you can do clicks without auto clicking, press it again to resume script
; 		   Press Alt + R to reload the script (useful for reloading the script after editing it while it's already running)

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.

LButton::SetTimer, ClickIt, 200
MButton::SetTimer, ClickIt, Off
ClickIt:
Click
Return 

RButton::SetTimer, RClickIt, 200
End::SetTimer, RClickIt, Off
RClickIt:
Click, R
Return	
	
~F6::suspend
!r::Reload